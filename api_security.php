<?php

// We want to make sure the information we receive is genuine
// First, we need to check if the request itself makes sense
// We need to verify the signature the source created for the requests content
// If some error, we want to store the ip to track if someone is fishing for information

class api_security
{

    // A request should contain the following items
    // - ip address that is not banned
    // - only allow POST requests
    // - application key in header (uuidv4), do we expect it?
    // - valid json in body
    // - device key (uuidv4)
    // - timestamp not in future nor older then...
    // - valid signature
    public static function checkRequest(): void
    {
        // - ip address that is not banned
        if (!self::checkIPAddress()) {
            self::generateError('429 Too Many Requests', false);
        }

        // - only allow POST requests
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            self::generateError('405 Method Not Allowed', false);
        }

        // - application key in header (uuidv4), do we expect it?
        if (!isset($_SERVER['HTTP_X_APPLICATION_KEY']) || !api_uuidv4::check($_SERVER['HTTP_X_APPLICATION_KEY']) || !isset(APP_KEYS[$_SERVER['HTTP_X_APPLICATION_KEY']])) {
            self::generateError('400 Bad Request (HTTP_X_APPLICATION_KEY)', false);
        }

        // - device key
        if (!isset($_POST['device']) || !api_uuidv4::check($_POST['device'])) {
            self::generateError('400 Bad Request (device)', false);
        }

        // - timestamp not in future nor older then... (It is sent as string to keep the swift code clean)
        if (!isset($_POST['timestamp']) || !self::checkTimestamp($_POST['timestamp'])) {
            self::generateError('400 Bad Request (timestamp)', false);
        }

        // - signature key
        if (!isset($_POST['signature']) || !self::checkSignature()) {
            self::generateError('400 Bad Request (signature)', false);
        }
    }

    // Otherwise we generate an error, log the ip address/time and stop all processing
    public static function generateError(string $error, bool $log = true): void
    {
        $stmt = api_database::prepare('INSERT INTO security_failed_attempts (ip) VALUE (?)');
        $stmt->bind_param('s', $_SERVER['REMOTE_ADDR']);
        $stmt->execute();
        api_database::check();
        $stmt->close();

        if ($log) {
            trigger_error($error);
        }

        header('HTTP/1.0 ' . $error);
        exit;
    }

    // Check how many failed attempts are made within a specified time
    private static function checkIPAddress(): bool
    {
        $result = null;
        $stmt = api_database::prepare('SELECT COUNT(added) >= ' . FAILED_ATTEMPTS_NUMBER . ' FROM security_failed_attempts WHERE ip = ? and added > NOW() - INTERVAL ' . FAILED_ATTEMPTS_MINUTES . ' MINUTE');
        $stmt->bind_param('s', $_SERVER['REMOTE_ADDR']);
        $stmt->execute();
        $stmt->bind_result($result);
        $stmt->fetch();
        $stmt->close();

        if ($result == 1) {
            return false;
        }

        return true;
    }

    // Check if timestamp is within bounds (we are working with UTC timestamps!)
    private static function checkTimestamp(string $timestampString): bool
    {
        $nowTimestamp = microtime(true);
        $timestamp = $timestampString / 1000;

        // In the future?
        if ($timestamp - $nowTimestamp > TIMESTAMP_AGE_SECONDS) {
            return false;
        }

        // In the past?
        if ($nowTimestamp - $timestamp > TIMESTAMP_AGE_SECONDS) {
            return false;
        }

        return true;
    }


    // When receiving data, check if the signature is valid
    private static function checkSignature(): bool
    {
        // Get signature and remove it from the array 
        $signature_check_1 = $_POST['signature'];

        // We are stripping the signature, but we do expect it to be the last item :-) 
        $querystring = file_get_contents('php://input');
        $querystring = substr($querystring, 0, strpos($querystring, '&signature='));

        // Re-calculate signature 
        $secret = APP_KEYS[$_SERVER['HTTP_X_APPLICATION_KEY']]['secret'];
        $signature_check_2 = api_request::signature($querystring, $secret);

        // Do signatures match?
        if ($signature_check_1 == $signature_check_2) {
            return true;
        }

        return false;
    }

    // Deprecated, moved to api_request::send
    public static function sendParamaters(string $appKey, string $appSecret, string $url, array $data): array
    {
        trigger_error('Deprecated, moved to api_request::send');
        return api_request::send($appKey, $appSecret, $url, $data);
    }

    //Deprecated, moved to api_request::signature
    private static function generateSignature(string $querystring, string $secret): string
    {
        trigger_error('Deprecated, moved to api_request::signature');
        return api_request::signature($querystring, $secret);
    }

    // Deprecated, moved to api_uuidv4::generate
    public static function uuidv4(): string
    {
        trigger_error('Deprecated, moved to api_uuidv4::generate');
        return api_uuidv4::generate();
    }

    // Deprecated, moved to api_uuidv4::check
    public static function checkUUIDv4(string $UUIDv4): bool
    {
        trigger_error('Deprecated, moved to api_uuidv4::check');
        return api_uuidv4::check($UUIDv4);
    }
}
