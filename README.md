# API Generics - PHP

Facilitates generic functions for the other APIs. Handling of security and database operations.

## Installation
This repository is used as submodule:  
`git submodule add https://gitlab.com/rizkysly-api/api-generics.git`  

Import the `database.sql` file into your mysql database.  

Include the following lines to your project
```php
include(__DIR__ . '/api-generics/api_database.php');
include(__DIR__ . '/api-generics/api_request.php');
include(__DIR__ . '/api-generics/api_security.php');
include(__DIR__ . '/api-generics/api_uuidv4.php');
api_security::checkRequest();
```

Constants for database, application keys and application secrets are defined by the main API.

## api_database
Will make sure only one database connection is used and reports errors.

```php
// Prepare database query and return the statement object
api_database::prepare($query) : mysqli_stmt

// Execute query without bind parameters and return if successful
api_database::query($query) : bool

// Report database errors after executing a statement
api_database::check()
```

## api_request
Create requests to communicate with api's using these generic classes

```php
// Generates the signature and sends the request to the supplied url
// The data array is converted to post fields and values
// Returns an array with the server response and http status code
api_request::send(string $appKey, string $appSecret, string $url, array $data) : array
```

## api_security
Checks all incoming requests. It will check for abuse, request method, application key (uuidv4), device key (uuidv4), timestamp and signature.  

All requests are post requests. The application key is included in the request header while all other information is sent by post values. The signature is a hmac hash based on all post values and the application secret.

**Headers**  
>>>
'X-APPLICATION-KEY: 00000000-0000-0000-0000-000000000000'  
'Content-Type:application/x-www-form-urlencoded'
>>>

**Required post values**  
>>>
device=00000000-0000-0000-0000-000000000000  
timestamp=0  
signature=hmac_hash
>>>

**Response codes**
>>>
200 Success  
429 Too Many Requests  
405 Method Not Allowed  
400 Bad Request  
5xx Server error  
>>>

## api_uuidv4
Check and generate uuidv4 strings

```php
// Generate a random uuidv4
api_uuidv4::generate()

// Check if a string is a valid uuidv4
api_uuidv4::check(string $UUIDv4) : bool
```