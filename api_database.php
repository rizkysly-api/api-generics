<?php

// Just handle the boring database stuff. 
// Create a connection if not connected
// Make sure we get an error is something is 
//  wrong with the statement or the execution of one

class api_database
{
    private static $mysqli = false;

    private static function init()
    {
        //Check if database credentials are filled
        if (DB_USERNAME == '' || DB_PASSWORD == '' || DB_DATABASE == '') {
            die('No database connection without correct database credentials');
        }

        if (self::$mysqli === false) {
            self::$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

            if (!self::$mysqli) {
                die('Cannot connect to database');
            }

            self::$mysqli->set_charset("utf8mb4");
        }
    }

    public static function prepare(string $query) : mysqli_stmt
    {
        self::init();

        $stmt = self::$mysqli->prepare($query);

        if (self::$mysqli->errno > 0 && self::$mysqli->errno != 2014) {
            trigger_error(self::$mysqli->error);
        }

        $stmt->store_result();

        return $stmt;
    }

    // Execute a simple query for INSERT, UPDATE or DELETE
    public static function query(string $query) : bool
    {
        self::init();

        return self::$mysqli->query($query);
    }

    // Log errors that occure while executing a query
    public static function check() : void
    {
        if (self::$mysqli->error) {
            trigger_error(self::$mysqli->error);
        }
    }
}