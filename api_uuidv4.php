<?php

// Some uuidv4 functions

class api_uuidv4
{

    // The apy key and the device key are uuidv4's, so lets check those
    public static function check(string $UUIDv4) : bool
    {
        $UUIDv4Format = '/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i';
        if (preg_match($UUIDv4Format, $UUIDv4)) {
            return true;
        }

        return false;
    }

    // Generate a random uuidv4
    // https://stackoverflow.com/a/15875555
    public static function generate() : string
    {
        $data = random_bytes(16);
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }
}