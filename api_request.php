<?php

// Create requests to api's using this project

class api_request
{

    // Sending requires signing the parameters, so why not do it all at once
    public static function send(string $appKey, string $appSecret, string $url, array $parameters): array
    {
        //Add timestamp and signature
        $parameters['device'] = (isset($parameters['device'])) ? $parameters['device'] : APP_KEY;
        $parameters['timestamp'] = round(microtime(true) * 1000);

        $querystring = http_build_query($parameters);
        $querystring .= '&signature=' . self::signature($querystring, $appSecret);

        $headers = array(
            'X-APPLICATION-KEY:' . $appKey,
            'Content-Type:application/x-www-form-urlencoded'
        );

        //Send them on
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $querystring);
        $response = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return [$response, $httpcode];
    }

    //Generate the signature, based on the contents of the data
    public static function signature(string $querystring, string $secret): string
    {
        return hash_hmac('sha512', $querystring, $secret, false);
    }
}
